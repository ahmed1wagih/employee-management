<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EmployeeController;
use App\Http\Middleware\Custom\CSVContentTypeValidator;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/


Route::controller(EmployeeController::class)->prefix('employee')->group(function()
{
   Route::post('', 'patch')->middleware(CSVContentTypeValidator::class);
   Route::get('', 'list');
   Route::get('{id}', 'show')->where('id', '[0-9]+');
   Route::delete('/{id}', 'delete')->where('id', '[0-9]+');
});
