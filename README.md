# Employee Batch Import & Management API

This project is a RESTful API for managing employee data, including batch import of employees from CSV files.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
    - [API Endpoints](#api-endpoints)

## Features

- Import employee data from CSV files.
- List, retrieve, and delete employee records.


## Getting Started

### Prerequisites

Before running the project, ensure you have the following prerequisites installed on your system:

- PHP (v8.1+ recommended)
- Composer
- MySQL or MariaDB
- PHPUnit (for testing)

### Installation
You can download the compressed file or the project 
or clone via git repo https://gitlab.com/ahmed1wagih/employee-management

Via the cmd line in the project directory
1. composer install
2. cp .env.example .env
3. Update the DB connection credentials with yours <br/>(DB_CONNECTION, DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD)
4. php artisan key:generate
5. php artisan migrate
6. php artisan serve


## Features
Your Employee API should now be running locally at http://localhost:8000.

A postman collection is also attached within main directory of the zip file / repo to make this easier <br/>
with the name of postman_collection.json <br/>

Set/Replace the {{host}} variable value with http://localhost:8000/api or another port if you planning to serve on another port