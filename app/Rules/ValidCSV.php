<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ValidCSV implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        //checks if the input can be successfully parsed as CSV, valid format and rows count
        $csv = @str_getcsv($value);

        if(! $csv) $fail(':attribute can not be parsed to csv');
        if(! (is_array($csv) && count($csv))) $fail(':attribute is not valid csv');
    }
}
