<?php

namespace App\Services;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class EmployeeService
{
    private Model|Builder $model;


    public function __construct()
    {
        $this->model = new Employee();
    }


    public function patch($data) :array
    {
        $chunkSize = 1000; // Adjust this to your needs
        $csvRows = explode(PHP_EOL, $data);

        $chunks = array_chunk($csvRows, $chunkSize);

        //declare the variables only once before the loop instead of declaring it multiple times for each loop
        $arrayMap = [];
        $queryArray = [];
        $counters = ['Total' => 0, 'Succeeded' => 0, 'Failed' => 0];

        foreach ($chunks as $chunkKey => $chunk)
        {
            foreach ($chunk as $rowKey => $row)
            {
                if(array_key_first($chunks) == $chunkKey && array_key_first($chunk) == $rowKey)
                {
                    $arrayMap = self::arrayMap($row);
                    continue;
                }

                $counters['Total']++;

                $rowData = str_getcsv($row);

                if(count($rowData) != count($arrayMap))
                {
                    $counters['Failed']++;
                    continue;
                }

                //matching key with the right value
                foreach($arrayMap as $arrayKey => $arrayValue)
                {
                    $queryArray[$arrayKey] = $rowData[$arrayValue];
                }

                //handling different data types that could fail the query, and giving the quick stats for the patching process via count for each case
                try
                {
                    if(isset($queryArray['emp_id'])) $this->model::updateOrCreate(['emp_id' => $queryArray['emp_id']], $queryArray);
                    $counters['Succeeded']++;
                }
                catch(\Exception $exception)
                {
                    $counters['Failed']++;
                }
            }
        }

        return $counters;
    }


    public function list($request) :array
    {
        $data['perPage'] = isset($request->perPage) ? intval($request->perPage) : 20;
        $data['feed'] = $this->model->paginate($data['perPage']);

        return $data;
    }


    public function show() :array
    {
        $id = request()->route('id');
        $model = $this->model->find($id);

        if($model) $data['status'] = 'Found';
        else $data['status'] = 'Not found';

        $data['model'] = $model;

        return $data;
    }


    public function delete() :array
    {
        $id = request()->route('id');
        $exists = Employee::where('emp_id', $id)->select('emp_id')->first();
        if($exists)
        {
            $exists->delete();
            $data['msg'] = 'Deleted';
        }
        else $data['msg'] = 'Not found';


        return $data;
    }


    private static function arrayMap(string $columns) :array
    {
        // getting the key => value for each column in case of the order is not static, to avoid mis-assigning for data
        $array = [];

        $columns = str_getcsv($columns);

        foreach($columns as $key => $column)
        {
            switch($column)
            {
                case 'Emp ID':
                    $array['emp_id'] = $key;
                    break;
                case 'Name Prefix':
                    $array['name_prefix'] = $key;
                    break;
                case 'First Name':
                    $array['first_name'] = $key;
                    break;
                case 'Middle Initial':
                    $array['middle_initial'] = $key;
                    break;
                case 'Last Name':
                    $array['last_name'] = $key;
                    break;
                case 'Gender':
                    $array['gender'] = $key;
                    break;
                case 'E Mail':
                    $array['email'] = $key;
                    break;
                case 'Date of Birth':
                    $array['date_of_birth'] = $key;
                    break;
                case 'Time of Birth':
                    $array['time_of_birth'] = $key;
                    break;
                case 'Age in Yrs.':
                    $array['age_in_years'] = $key;
                    break;
                case 'Date of Joining':
                    $array['date_of_joining'] = $key;
                    break;
                case 'Age in Company (Years)':
                    $array['age_in_company'] = $key;
                    break;
                case 'Phone No. ':
                    $array['phone_no'] = $key;
                    break;
                case 'Place Name':
                    $array['place_name'] = $key;
                    break;
                case 'County':
                    $array['county'] = $key;
                    break;
                case 'City':
                    $array['city'] = $key;
                    break;
                case 'Zip':
                    $array['zip'] = $key;
                    break;
                case 'Region':
                    $array['region'] = $key;
                    break;
                case 'User Name':
                    $array['user_name'] = $key;
                    break;
            }
        }

        return $array;
    }
}