<?php

namespace App\Traits;

use Illuminate\Http\Exceptions\HttpResponseException;

trait Responser
{
    public function rJson($data=[], $status = 'success') :\Illuminate\Http\JsonResponse
    {
        $code = match ($status)
        {
            'success' => 200,
            'validation' => 422,
            'invalid_header' => 400,
            default => 200,
        };

        throw new HttpResponseException(response()->json([$data], $code));
    }
}