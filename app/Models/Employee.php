<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Employee extends Model
{
    protected $primaryKey = 'emp_id';


    protected $fillable = ['emp_id', 'name_prefix', 'first_name', 'middle_initial', 'last_name', 'gender', 'email', 'date_of_birth', 'time_of_birth', 'age_in_years', 'date_of_joining', 'age_in_company', 'phone_no', 'place_name', 'county', 'city', 'zip', 'region', 'user_name'];


    public $timestamps = false;


    protected function dateOfBirth(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => Carbon::parse($value)->format('Y-m-d'),
            get: fn (string $value) => Carbon::parse($value)->format('d-m-Y')
        );
    }


    protected function timeOfBirth(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => Carbon::parse($value)->format('H:i:s'),
            get: fn (string $value) => Carbon::parse($value)->format('h:i:s A')
        );
    }


    protected function dateOfJoining(): Attribute
    {
        return Attribute::make(
            set: fn (string $value) => Carbon::parse($value)->format('Y-m-d'),
            get: fn (string $value) => Carbon::parse($value)->format('d-m-Y')
        );
    }
}
