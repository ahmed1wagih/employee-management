<?php

namespace App\Http\Controllers;

use App\Http\Requests\Employee\ListRequest;
use App\Http\Requests\Employee\PatchRequest;
use App\Services\EmployeeService;

class EmployeeController extends Controller
{
    private EmployeeService $employeeService;


    public function __construct(EmployeeService $promoService)
    {
        $this->employeeService = $promoService;
    }


    public function patch(PatchRequest $request) :\Illuminate\Http\JsonResponse
    {
        $patch = $this->employeeService->patch($request->data);
        return $this->rJson(['msg' => 'Patched', 'stats' => $patch]);
    }


    public function list(ListRequest $request) :\Illuminate\Http\JsonResponse
    {
        $data = $this->employeeService->list($request);
        return $this->rJson($data);
    }


    public function show():\Illuminate\Http\JsonResponse
    {
        $data = $this->employeeService->show();
        return $this->rJson($data);
    }


    public function delete() :\Illuminate\Http\JsonResponse
    {
        $data = $this->employeeService->delete();
        return $this->rJson($data);
    }
}
