<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\ParentFormRequest;
use App\Rules\ValidCSV;
use Illuminate\Http\Request;

class PatchRequest extends ParentFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function __construct()
    {
        parent::__construct();
        request()->merge(['data' => request()->getContent()]);
    }

    public function rules(): array
    {
        return
        [
            'data' => ['required', new ValidCSV]
        ];
    }
}
