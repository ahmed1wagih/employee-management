<?php

namespace App\Http\Requests\Employee;

use App\Http\Requests\ParentFormRequest;

class ListRequest extends ParentFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return
        [
            'perPage' => ['sometimes', 'integer', 'gt:0']
        ];
    }
}
