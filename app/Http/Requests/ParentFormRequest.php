<?php

namespace App\Http\Requests;

use App\Traits\Responser;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;


class ParentFormRequest extends FormRequest
{
    use Responser;


    public function failedValidation(Validator $validator)
    {
        return $this->rJson(['msg' => 'Invalid payload', 'payload' => $validator->getMessageBag()], 'validation');
    }
}
