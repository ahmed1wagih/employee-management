<?php

namespace App\Http\Middleware\Custom;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\Responser;

class CSVContentTypeValidator
{
    use Responser;
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // Check if the Content-Type header is 'text/csv'
        if ($request->header('Content-Type') !== 'text/csv')
        {
            return $this->rJson(['msg' => 'Invalid Content-Type'], 'invalid_header');
        }

        return $next($request);
    }
}
